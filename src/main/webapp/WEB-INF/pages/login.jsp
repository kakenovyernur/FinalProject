<!DOCTYPE html>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@page session="false"%>


<head>
  <title>Login Page</title>
  <jsp:include page="../bootstrapInclude/bootstrap_include.jsp"/>
</head>

<body class="login-img3-body">

  <div class="container">

    <form:form action="/authorization" method="post" modelAttribute="users" class="login-form" >
      <div class="login-wrap">
        <p class="login-img"><i class="icon_lock_alt"></i></p>
        <div class="input-group">
          <span class="input-group-addon"><i class="icon_profile"></i></span>
          <form:input path="login" required="true" type="text" class="form-control" placeholder="Username"/>
        </div>
        <div class="input-group">
          <span class="input-group-addon"><i class="icon_key_alt"></i></span>
          <form:input path="password" required="true" type="password" class="form-control" placeholder="Password"/>
        </div>

        <button class="btn btn-primary btn-lg btn-block" type="submit">Login</button>
      </div>
    </form:form>
    <div class="text-right">
      <div class="credits">
          <!--
            All the links in the footer should remain intact.
            You can delete the links only if you purchased the pro version.
            Licensing information: https://bootstrapmade.com/license/
            Purchase the pro version form: https://bootstrapmade.com/buy/?theme=NiceAdmin
          -->
          
        </div>
    </div>
  </div>


</body>

</html>
