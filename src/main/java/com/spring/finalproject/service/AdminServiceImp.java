package com.spring.finalproject.service;

import com.spring.finalproject.dao.AdminDao;
import com.spring.finalproject.model.Items;
import com.spring.finalproject.model.Roles;
import com.spring.finalproject.model.Transaction_history;
import com.spring.finalproject.model.Users;
import org.hibernate.Session;
import org.springframework.stereotype.Service;

import javax.persistence.Table;
import javax.transaction.Transactional;
import java.util.List;

@Service
public class AdminServiceImp implements AdminService {
    private AdminDao adminDao;

    public void setAdminDao(AdminDao adminDao) {
        this.adminDao = adminDao;
    }

    @Override
    @Transactional
    public List<Users> usersList() {
        return this.adminDao.usersList();
    }

    @Override
    @Transactional
    public Users getUserByLoginAndPassword(String login, String password) {
        return this.adminDao.getUserByLoginAndPassword(login,password);
    }

    @Override
    @Transactional
    public Users getAdminById(int id) {
        return this.adminDao.getAdminById(id);
    }

    @Override
    @Transactional
    public List<Roles> rolesList() {
        return this.adminDao.rolesList();
    }

    @Override
    @Transactional
    public List<Roles> rolesListById() {
        return this.adminDao.rolesListById();
    }

    @Override
    @Transactional
    public void addItem(Items items) {
        this.adminDao.addItem(items);
    }

    @Override
    @Transactional
    public void deleteItem(Items item) {
      this.adminDao.deleteItem(item);
    }

    @Override
    @Transactional
    public void editItem(Items item) {
        this.adminDao.editItem(item);
    }

    @Override
    @Transactional
    public Items getItemById(int id) {
        return this.adminDao.getItemById(id);
    }

    @Override
    @Transactional
    public List<Items> itemsList() {
        return this.adminDao.itemsList();
    }

    @Override
    @Transactional
    public void addCashier(Users user) {
         this.adminDao.addCashier(user);
    }

    @Override
    @Transactional
    public void deleteCashier(Users user) {
          this.adminDao.deleteCashier(user);
    }

    @Override
    @Transactional
    public void editCashier(Users user) {
     this.adminDao.editCashier(user);
    }

    @Override
    @Transactional
    public Users getCashierById(int id) {
        return this.adminDao.getCashierById(id);
    }

    @Override
    @Transactional
    public List<Users> cashierList() {
        return this.adminDao.cashierList();
    }

    @Override
    @Transactional
    public void addTransaction(Transaction_history transaction_history) {
        this.adminDao.addTransaction(transaction_history);
    }

    @Override
    @Transactional
    public void updateItemAmount(Items items,int amount) {
        this.adminDao.updateItemAmount(items,amount);
    }

    @Override
    @Transactional
    public List<Transaction_history> transactionHistories() {
        return this.adminDao.transactionHistories();
    }

    @Override
    @Transactional
    public List<Transaction_history> transactionListById(int id) {
        return this.adminDao.transactionListById(id);
    }
}
