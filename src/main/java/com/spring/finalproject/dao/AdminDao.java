package com.spring.finalproject.dao;
import com.spring.finalproject.model.*;

import java.util.List;

public interface AdminDao {
    public List<Users> usersList();
    public Users getUserByLoginAndPassword(String login,String password);

    public Users getAdminById(int id);
    public List<Roles> rolesList();
    public List<Roles> rolesListById();

    public void addItem(Items items);
    public void deleteItem(Items item);
    public void editItem(Items item);
    public Items getItemById(int id);
    public List<Items> itemsList();

    public void addCashier(Users user);
    public void deleteCashier(Users user);
    public void editCashier(Users user);
    public Users getCashierById(int id);
    public List<Users> cashierList();

    public void addTransaction(Transaction_history transaction_history);
    public void updateItemAmount(Items items,int amount);


    public List<Transaction_history> transactionHistories();
    public List<Transaction_history> transactionListById(int id);
}
