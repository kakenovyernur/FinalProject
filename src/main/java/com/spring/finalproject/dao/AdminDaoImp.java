package com.spring.finalproject.dao;

import com.spring.finalproject.model.Items;
import com.spring.finalproject.model.Roles;
import com.spring.finalproject.model.Transaction_history;
import com.spring.finalproject.model.Users;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class AdminDaoImp implements AdminDao {
    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Users> usersList() {
        Session session = sessionFactory.getCurrentSession();
        List<Users> usersList = session.createQuery("FROM Users").list();
        return usersList;
    }

    @Override
    public Users getUserByLoginAndPassword(String login, String password) {
        for (Users users: usersList()){
            if(login.equals(users.getLogin()) && password.equals(users.getPassword())){
                return users;
            }
        }
        return null;
    }

    @Override
    public Users getAdminById(int id) {
        Session session = sessionFactory.getCurrentSession();
        Users users = (Users) session.get(Users.class,new Integer(id));
        return users;
    }

    @Override
    public List<Roles> rolesList() {
        Session session = sessionFactory.getCurrentSession();
        List<Roles> rolesList = session.createQuery("FROM Roles").list();
        return rolesList;
    }

    @Override
    public List<Roles> rolesListById() {
        Session session = sessionFactory.getCurrentSession();
        List<Roles> rolesListById = session.createQuery("FROM Roles WHERE role_id = :id").setParameter("id",2).list();
        return rolesListById;
    }

    @Override
    public void addItem(Items items) {
        Session session = sessionFactory.getCurrentSession();
        session.save(items);
    }

    @Override
    public void deleteItem(Items item) {
        Session session = sessionFactory.getCurrentSession();

        session.delete(item);
    }

    @Override
    public void editItem(Items item) {
        Session session = sessionFactory.getCurrentSession();
        session.update(item);
    }

    @Override
    public Items getItemById(int id) {
        Session session = sessionFactory.getCurrentSession();
        Items items = (Items) session.get(Items.class,new Integer(id));
        return items;
    }

    @Override
    public List<Items> itemsList() {
        Session session = sessionFactory.getCurrentSession();
        List<Items> itemsList = session.createQuery("from Items").list();
        return itemsList;
    }

    @Override
    public void addCashier(Users user) {
        Session session = sessionFactory.getCurrentSession();
        session.save(user);
    }

    @Override
    public void deleteCashier(Users user) {
        Session session = sessionFactory.getCurrentSession();
        session.delete(user);
    }

    @Override
    public void editCashier(Users user) {
        Session session = sessionFactory.getCurrentSession();
        session.update(user);
    }

    @Override
    public Users getCashierById(int id) {
        Session session = sessionFactory.getCurrentSession();
        Users users = (Users) session.get(Users.class,new Integer(id));
        return users;
    }

    @Override
    public List<Users> cashierList() {
        Session session = sessionFactory.getCurrentSession();
        List<Users> cashierList = session.createQuery("FROM Users WHERE role_id =:id ").setParameter("id",2).list();
        return cashierList;
    }

    @Override
    public void addTransaction(Transaction_history transaction_history) {
        Session session = sessionFactory.getCurrentSession();
        session.save(transaction_history);
    }

    @Override
    public void updateItemAmount(Items items,int amount) {
        Session session = sessionFactory.getCurrentSession();
        items.setAmounts(amount);
        session.update(items);
    }

    @Override
    public List<Transaction_history> transactionHistories() {
        Session session = sessionFactory.getCurrentSession();
        List<Transaction_history> transaction_histories = session.createQuery("FROM Transaction_history").list();
        return transaction_histories;
    }

    @Override
    public List<Transaction_history> transactionListById(int id) {
        Session session = sessionFactory.getCurrentSession();
        List<Transaction_history> transactionListById = session.createQuery("FROM Transaction_history WHERE users.id = :id").setParameter("id",id).list();
        return transactionListById;
    }

}
