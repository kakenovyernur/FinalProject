package com.spring.finalproject.model;

import javax.persistence.*;

@Entity
@Table(name = "items")
public class Items {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "item_id")
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "universal_product_code")
    private long universal_product_code;

    @Column(name = "price")
    private int price;

    @Column(name = "amounts")
    private int amounts;

    public Items() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getUniversal_product_code() {
        return universal_product_code;
    }

    public void setUniversal_product_code(long universal_product_code) {
        this.universal_product_code = universal_product_code;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getAmounts() {
        return amounts;
    }

    public void setAmounts(int amounts) {
        this.amounts = amounts;
    }

    @Override
    public String toString() {
        return "Items{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", universal_product_code=" + universal_product_code +
                ", price=" + price +
                ", amounts=" + amounts +
                '}';
    }
}
