package com.spring.finalproject.controller;

import com.spring.finalproject.model.Items;
import com.spring.finalproject.model.Transaction_history;
import com.spring.finalproject.model.Users;
import com.spring.finalproject.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;

import java.util.Date;

@Controller
@SessionAttributes("cashierModel")
@RequestMapping(value = "/cashier")
public class CashierController {
    private AdminService adminService;
    @Autowired
    @Qualifier("adminService")
    public void setAdminService(AdminService adminService) {
        this.adminService = adminService;
    }
    @RequestMapping(value = "/login")
    public String index1(ModelMap modelMap){
        if(modelMap.get("cashierModel")==null) {
            modelMap.addAttribute("cashier", new Users());
            return "cashierlogin";
        }else return "redirect:/cashier/profile";
    }
    @RequestMapping(value = "/auth", method = RequestMethod.POST)
    public String authPost(@ModelAttribute("users")Users users,ModelMap modelMap){
        Users users1 = adminService.getUserByLoginAndPassword(users.getLogin(),users.getPassword());
        if(users1!=null && users1.getRoles().getId() == 2){
            modelMap.addAttribute("cashierModel",users1);
            return "redirect:/cashier/profile";
        } else return "redirect:/cashier/login";
    }
    @RequestMapping(value = "/auth")
    public String authGet(@ModelAttribute("users")Users users,ModelMap modelMap){
        if(modelMap.get("cashierModel")!=null){
            return "redirect:/cashier/profile";
        }else return "redirect:/cashier/login";
    }
    @RequestMapping(value = "/profile")
    public String profileCashier(ModelMap modelMap){
        modelMap.addAttribute("cashier",adminService.getCashierById(((Users)modelMap.get("cashierModel")).getId()));
        return "cashier";
    }
    @RequestMapping(value = "edit/cashier/{id}")
    public String editCashier(@PathVariable("id") int id,ModelMap modelMap){
        modelMap.addAttribute("cashier",adminService.getCashierById(id));
        modelMap.addAttribute("rolesList",adminService.rolesListById());
        return "editCashPage";
    }
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public String edit(@ModelAttribute("cashier") Users users){
        adminService.editCashier(users);
        return "redirect:/cashier/profile";
    }
    @RequestMapping(value = "/selling")
    public String Sell(ModelMap modelMap){
        modelMap.addAttribute("transaction_history",new Transaction_history());
        modelMap.addAttribute("itemsList",adminService.itemsList());
        modelMap.addAttribute("cashier",adminService.getCashierById(((Users)modelMap.get("cashierModel")).getId()));
        return "sellingPage";
    }
    @RequestMapping(value = "/sell/good",method = RequestMethod.POST)
    public String sellGood(@ModelAttribute("transaction_history") Transaction_history transaction_history, ModelMap modelMap){
        Items items = adminService.getItemById(transaction_history.getItems().getId());
        if(items.getAmounts()<transaction_history.getAmount() || transaction_history.getAmount()==0){
            modelMap.addAttribute("msg","amount more than items-amount");
            return "redirect:/cashier/selling";
        }else {
            Date date = new Date();
            transaction_history.setTransaction_time(date);
            adminService.addTransaction(transaction_history);
            int amount = items.getAmounts()-transaction_history.getAmount();
            adminService.updateItemAmount(items,amount);
            int id = (((Users)modelMap.get("cashierModel")).getId());
            modelMap.addAttribute("transaction_historyList", adminService.transactionListById(id));
            return "redirect:/cashier/all/selling/";
        }
    }
    @RequestMapping(value = "/sell/good")
    public String sellGoodGet(ModelMap modelMap){
        int id = (((Users)modelMap.get("cashierModel")).getId());
        modelMap.addAttribute("transaction_historyList", adminService.transactionListById(id));
        return "redirect:/cashier/all/selling/";
    }
    @RequestMapping(value = "/all/selling")
    public String sellProductGet(ModelMap modelMap){
        int id = (((Users)modelMap.get("cashierModel")).getId());
        modelMap.addAttribute("cashier",adminService.getCashierById(id));
        modelMap.addAttribute("transaction_historyList", adminService.transactionListById(id));
        return "allSell";
    }

    @RequestMapping(value = "/logout")
    public String log(SessionStatus status){
        status.setComplete();
        return "redirect:/";
    }

}
