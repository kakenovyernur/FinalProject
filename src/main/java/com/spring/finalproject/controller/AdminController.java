package com.spring.finalproject.controller;

import com.spring.finalproject.model.Items;
import com.spring.finalproject.model.Users;
import com.spring.finalproject.service.AdminService;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;

@Controller
@SessionAttributes("adminModel")
public class AdminController {
    private AdminService adminService;
    @Autowired
    @Qualifier("adminService")
    public void setAdminService(AdminService adminService) {
        this.adminService = adminService;
    }

    @RequestMapping(value = "/")
    public String index(){
        return "index";
    }
    @RequestMapping(value = "/admin")
    public String login(ModelMap modelMap){
        if(modelMap.get("adminModel")==null) {
            modelMap.addAttribute("users", new Users());
            return "login";
        }else return "redirect:/profile";
    }
    @RequestMapping(value = "/authorization", method = RequestMethod.POST)
    public String auth(@ModelAttribute("users")Users users,ModelMap modelMap){
        Users users1 = adminService.getUserByLoginAndPassword(users.getLogin(),users.getPassword());
        if(users1!=null && users1.getRoles().getId()==1){
            modelMap.addAttribute("adminModel",users1);
            return "redirect:/profile";
        } else return "redirect:/admin";
    }
    @RequestMapping(value = "/authorization")
    public String authget(@ModelAttribute("users")Users users,ModelMap modelMap){
        if(modelMap.get("adminModel")!=null){
            return "redirect:/profile";
        }else return "redirect:/";
    }
    @RequestMapping(value = "/profile")
    public String admin(ModelMap modelMap){
        modelMap.addAttribute("admin",adminService.getAdminById(((Users)modelMap.get("adminModel")).getId()));
        return "admin";
    }
    @RequestMapping(value = "/allitems")
    public String allItems(@ModelAttribute("items")Items items, ModelMap modelMap){
        modelMap.addAttribute("itemsList",adminService.itemsList());
        return "allitems";
    }
    @RequestMapping(value = "/additems")
    public String Item(ModelMap modelMap){
        modelMap.addAttribute("items",new Items());
        return "additems";
    }
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String addItem(@ModelAttribute("items") Items items){
        String g = String.valueOf(items.getUniversal_product_code());
        if(g.length()>10 || g.length()<10){
            return "redirect:/additems";
        }
        if(items.getAmounts()==0){
            return "redirect:/additems";
        }
        adminService.addItem(items);
        return "redirect:/allitems";
    }
    @RequestMapping(value = "/delete/item/{id}")
    public String deleteItem(@PathVariable("id") int id){
        Items items = adminService.getItemById(id);
        adminService.deleteItem(items);
        return "redirect:/allitems";
    }
    @RequestMapping(value = "/edit/item/{id}")
    public String editItem(@PathVariable("id") int id,ModelMap modelMap){
        Items items = adminService.getItemById(id);
        modelMap.addAttribute("item",items);
        return "edititems";
    }
    @RequestMapping(value = "/edit/item")
    public String updateItem(@ModelAttribute("items")Items items){
        String g = String.valueOf(items.getUniversal_product_code());
        if(g.length()>10 || g.length()<10){
            return "redirect:/allitems";
        }
        if(items.getAmounts()==0){
            return "redirect:/allitems";
        }
        adminService.editItem(items);
        return "redirect:/allitems";
    }
    @RequestMapping(value = "/allcashiers")
    public String allCashiers(ModelMap modelMap){
        modelMap.addAttribute("cashierList", adminService.cashierList());
        return "allcashiers";
    }
    @RequestMapping(value = "/edit/cashier/{id}")
    public String editCashier(@PathVariable("id") int id,ModelMap modelMap){
        modelMap.addAttribute("cashier",adminService.getCashierById(id));
        modelMap.addAttribute("rolesList",adminService.rolesList());
        return "editcashier";
    }
    @RequestMapping(value = "edit/cashier")
    public String editCash(@ModelAttribute("user") Users users){
        adminService.editCashier(users);
        return "redirect:/allcashiers";
    }
    @RequestMapping(value = "/addcashier")
    public String addCash(ModelMap modelMap){
        modelMap.addAttribute("user", new Users());
        modelMap.addAttribute("rolesList",adminService.rolesListById());
        return "addcashier";
    }
    @RequestMapping(value = "/add/cashier")
    public String addCashier(@ModelAttribute("user")Users users){
        adminService.addCashier(users);
        return "redirect:/allcashiers";
    }
    @RequestMapping(value = "delete/cashier/{id}")
    public String deleteCashier(@PathVariable("id")int id){
        adminService.deleteCashier(adminService.getCashierById(id));
        return "redirect:/allcashiers";
    }
    @RequestMapping(value = "/all/selling/product")
    public String sellProd(ModelMap modelMap){
        modelMap.addAttribute("transaction_histroryList",adminService.transactionHistories());
        return "allSellAdmin";
    }
    @RequestMapping(value = "/logout")
    public String commit(SessionStatus status){
        status.setComplete();
        return "redirect:/";
    }

}
