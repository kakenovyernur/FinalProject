<!DOCTYPE html>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="from" uri="http://www.springframework.org/tags/form" %>
<%@page session="false"%>
<head>
  <title>Admin</title>
  <jsp:include page="../bootstrapInclude/bootstrap_include.jsp"/>
</head>

<body>
  <!-- container section start -->
  <section id="container" class="">
    <header class="header dark-bg">
      <div class="toggle-nav">
        <div class="icon-reorder tooltips" data-original-title="Toggle Navigation" data-placement="bottom"><i class="icon_menu"></i></div>
      </div>
      <!--logo start-->
      <a href="#" class="logo"><span class="lite">Admin</span></a>
      <!--logo end-->
      <div class="top-nav notification-row">
        <!-- notificatoin dropdown start-->
        <ul class="nav pull-right top-menu">
          <!-- user login dropdown start-->
          <li class="dropdown">
            <a data-toggle="dropdown" class="dropdown-toggle" href="/profile">
                            <span class="profile-ava">
                                <img alt="" src="/resources/img/avatar-mini4.jpg">
                            </span>
                            <span class="username">Admin</span>
                            <b class="caret"></b>
                        </a>
            <ul class="dropdown-menu extended logout">
              <div class="log-arrow-up"></div>
              <li class="eborder-top">
                <a href="/profile"><i class="icon_profile"></i> My Profile</a>
              </li>
              <li>
                <a href="/logout"><i class="icon_key_alt"></i> Log Out</a>
              </li>
            </ul>
          </li>
          <!-- user login dropdown end -->
        </ul>
        <!-- notificatoin dropdown end-->
      </div>
    </header>
    <!--header end-->

    <!--sidebar start-->
    <aside>
      <div id="sidebar" class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu">
          <li class="active">
            <a class="" href="/profile">
                          <i class="icon_house_alt"></i>
                          <span>Home</span>
                      </a>
          </li>
          <li class="sub-menu">
            <a href="javascript:;" class="">
                          <i class="icon_document_alt"></i>
                          <span>Items</span>
                          <span class="menu-arrow arrow_carrot-right"></span>
                      </a>
            <ul class="sub">
              <li><a class="" href="/allitems">All Items</a></li>
            
              <li><a class="" href="/additems">Add Items</a></li>
            </ul>
          <li class="sub-menu">
            <a href="javascript:;" class="">
                          <i class="icon_table"></i>
                          <span>Cashier</span>
                          <span class="menu-arrow arrow_carrot-right"></span>
                      </a>
            <ul class="sub">
              <li><a class="" href="/allcashiers">All Cashier</a></li>
                <li><a class="" href="/addcashier">Add Cashier</a></li>
            </ul>
          </li>
          <li class="sub-menu">
            <a href="javascript:;" class="">
              <i class="icon_document_alt"></i>
              <span>Selling Product</span>
              <span class="menu-arrow arrow_carrot-right"></span>
            </a>
            <ul class="sub">
              <li><a class="" href="/cashier/selling">Selling goods</a></li>
              <li><a class="" href="/cashier/sell/product">All Selling goods</a></li>
            </ul>
          </li>
          

        </ul>
        <!-- sidebar menu end-->
      </div>
    </aside>
    <!--sidebar end-->

    <!--main content start-->
    <section id="main-content">
      <section class="wrapper">
        <!--overview start-->
              
          
        <!--Form-->
        <div class="row">
          <div class="col-lg-12">
            <h3 class="page-header"><i class="fa fa-laptop"></i>SupApp</h3>
            <ol class="breadcrumb">
              <li><i class="fa fa-home"></i><a href="#">Goods</a></li>
              <li><i class="fa fa-laptop"></i>SupApp</li>
            </ol>
          </div>
        </div>
           <div class="row">
          <div class="col-lg-12">
            <section class="panel">
              <header class="panel-heading">
                Edit Cashier
              </header>
              <div class="panel-body">
                <div class="form">
                  <form:form action="/edit/cashier" modelAttribute="cashier" method="post" class="form-validate form-horizontal" >
                    <div class="form-group ">
                      <form:label path="login" class="control-label col-lg-2">Name<span class="required">*</span></form:label>
                      <div class="col-lg-10">
                        <form:input path="login" required="true" class="form-control"  type="text"/>
                      </div>
                    </div>
                    <div class="form-group ">
                      <form:label path="password"  class="control-label col-lg-2">Password <span class="required">*</span></form:label>
                      <div class="col-lg-10">
                        <form:input path="password" required="true" class="form-control "/>
                      </div>
                    </div>
                    <div class="form-group ">
                      <form:label path="roles.name"  class="control-label col-lg-2">Role</form:label>
                      <div class="col-lg-10">
                      <form:select path="roles.id" class="form-control">
                        <form:options items="${rolesList}" itemLabel="name" itemValue="id"/>
                      </form:select>
                    </div>
                    </div>
                  <form:input path="id" value="${cashier.id}" type="hidden"/>
                    <div class="form-group">
                      <div class="col-lg-offset-2 col-lg-10">
                        <button class="btn btn-primary" type="submit">Confirm</button>
                      </div>
                    </div>
                  </form:form>
                </div>
              </div>
            </section>
          </div>
        </div>
    </section>
    <!--main content end-->
  </section>
  <!-- container section start -->
  <!-- Javascript files-->
  <jsp:include page="../bootstrapInclude/bootstrap_js.jsp"/>


</body>

</html>
