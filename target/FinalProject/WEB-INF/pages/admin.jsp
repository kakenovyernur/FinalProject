<!DOCTYPE html>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="from" uri="http://www.springframework.org/tags/form" %>
<%@page session="false"%>
<html lang="en">

<head>
  <title>Admin</title>
  <jsp:include page="../bootstrapInclude/bootstrap_include.jsp"/>
</head>

<body>
  <!-- container section start -->
  <section id="container" class="">


    <header class="header dark-bg">
      <div class="toggle-nav">
        <div class="icon-reorder tooltips" data-original-title="Toggle Navigation" data-placement="bottom"><i class="icon_menu"></i></div>
      </div>

      <!--logo start-->
      <a href="#" class="logo"><span class="lite">Admin</span></a>
      <!--logo end-->
      <div class="top-nav notification-row">
        <!-- notificatoin dropdown start-->
        <ul class="nav pull-right top-menu">
          <!-- user login dropdown start-->
          <li class="dropdown">
            <a data-toggle="dropdown" class="dropdown-toggle" href="/profile">
                            <span class="profile-ava">
                                <img alt="" src="/resources/img/avatar-mini4.jpg">
                            </span>
                            <span class="username">Admin</span>
                            <b class="caret"></b>
                        </a>
            <ul class="dropdown-menu extended logout">
              <div class="log-arrow-up"></div>
              <li class="eborder-top">
                <a href="/profile"><i class="icon_profile"></i> My Profile</a>
              </li>
              <li>
                <a href="/logout"><i class="icon_key_alt"></i> Log Out</a>
              </li>
            </ul>
          </li>
          <!-- user login dropdown end -->
        </ul>
        <!-- notificatoin dropdown end-->
      </div>
    </header>
    <!--header end-->

    <!--sidebar start-->
    <aside>
      <div id="sidebar" class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu">
          <li class="active">
            <a class="" href="/profile">
                          <i class="icon_house_alt"></i>
                          <span>Home</span>
                      </a>
          </li>
          <li class="sub-menu">
            <a href="javascript:;" class="">
                          <i class="icon_document_alt"></i>
                          <span>Items</span>
                          <span class="menu-arrow arrow_carrot-right"></span>
                      </a>
            <ul class="sub">
              <li><a class="" href="/allitems">All Items</a></li>
            
              <li><a class="" href="/additems">Add Items</a></li>
            </ul>
          </li>
          <li class="sub-menu">
            <a href="javascript:;" class="">
                          <i class="icon_table"></i>
                          <span>Cashier</span>
                          <span class="menu-arrow arrow_carrot-right"></span>
                      </a>
            <ul class="sub">
              <li><a class="" href="/allcashiers">All Cashier</a></li>
                <li><a class="" href="/addcashier">Add Cashier</a></li>
            </ul>
        </li>
            <li class="sub-menu">
                <a href="javascript:;" class="">
                    <i class="icon_table"></i>
                    <span>Selling Products</span>
                    <span class="menu-arrow arrow_carrot-right"></span>
                </a>
                <ul class="sub">
                    <li><a class="" href="/all/selling/product">All Selling Product</a></li>
                </ul>
          </li>

          

        </ul>
        <!-- sidebar menu end-->
      </div>
    </aside>
    <!--sidebar end-->

    <!--main content start-->
    <section id="main-content">
      <section class="wrapper">
        <!--overview start-->
              
          
        <!--Form-->
        <div class="row">
          <div class="col-lg-12">
            <h3 class="page-header"><i class="fa fa-laptop"></i>Supermarket</h3>
            <ol class="breadcrumb">
              <li><i class="fa fa-home"></i><a href="#">Goods</a></li>
              <li><i class="fa fa-laptop"></i>Supermarket</li>
            </ol>
          </div>
        </div>
          <!--TABLES-->
        <c:if test="${!empty admin}">
   <div class="row">
          <div class="col-sm-12">
            <section class="panel">
              <header class="panel-heading">
                Admin Information
              </header>
              <table class="table">
                <thead>
                  <tr>

                    <th>Login</th>
                    <th>Password</th>
                    <th>Role</th>

                  </tr>
                </thead>
                <tbody>
                  <tr>

                    <td>${admin.login}</td>
                    <td>${admin.password}</td>
                    <td>${admin.roles.name}</td>

                  </tr>
                </tbody>
              </table>
            </section>
          </div>
        </div>
        </c:if>
          <!--TABLES-->
      </section>
    
    </section>
    <!--main content end-->
  </section>
  <!-- container section start -->
  <!-- Javascript files-->
  <jsp:include page="../bootstrapInclude/bootstrap_js.jsp"/>


</body>

</html>
