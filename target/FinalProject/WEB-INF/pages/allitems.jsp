<!DOCTYPE html>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="from" uri="http://www.springframework.org/tags/form" %>
<%@page session="false"%>
<html lang="en">

<head>
  <title>Admin</title>
  <jsp:include page="../bootstrapInclude/bootstrap_include.jsp"/>
</head>

<body>
  <!-- container section start -->
  <section id="container" class="">


    <header class="header dark-bg">
      <div class="toggle-nav">
        <div class="icon-reorder tooltips" data-original-title="Toggle Navigation" data-placement="bottom"><i class="icon_menu"></i></div>
      </div>

      <!--logo start-->
      <a href="#" class="logo"><span class="lite">Admin</span></a>
      <!--logo end-->
      <div class="top-nav notification-row">
        <!-- notificatoin dropdown start-->
        <ul class="nav pull-right top-menu">
          <!-- user login dropdown start-->
          <li class="dropdown">
            <a data-toggle="dropdown" class="dropdown-toggle" href="/profile">
                            <span class="profile-ava">
                                <img alt="" src="/resources/img/avatar-mini4.jpg">
                            </span>
                            <span class="username">Admin</span>
                            <b class="caret"></b>
                        </a>
            <ul class="dropdown-menu extended logout">
              <div class="log-arrow-up"></div>
              <li class="eborder-top">
                <a href="/profile"><i class="icon_profile"></i> My Profile</a>
              </li>
              <li>
                <a href="/logout"><i class="icon_key_alt"></i> Log Out</a>
              </li>
            </ul>
          </li>
          <!-- user login dropdown end -->
        </ul>
        <!-- notificatoin dropdown end-->
      </div>
    </header>
    <!--header end-->

    <!--sidebar start-->
    <aside>
      <div id="sidebar" class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu">
          <li class="active">
            <a class="" href="/profile">
                          <i class="icon_house_alt"></i>
                          <span>Home</span>
                      </a>
          </li>
          <li class="sub-menu">
            <a href="javascript:;" class="">
                          <i class="icon_document_alt"></i>
                          <span>Items</span>
                          <span class="menu-arrow arrow_carrot-right"></span>
                      </a>
            <ul class="sub">
              <li><a class="" href="/allitems">All Items</a></li>
            
              <li><a class="" href="/additems">Add Items</a></li>
            </ul>
          </li>
          <li class="sub-menu">
            <a href="javascript:;" class="">
                          <i class="icon_table"></i>
                          <span>Cashier</span>
                          <span class="menu-arrow arrow_carrot-right"></span>
                      </a>
            <ul class="sub">
              <li><a class="" href="/allcashiers">All Cashier</a></li>
                <li><a class="" href="/addcashier">Add Cashier</a></li>
            </ul>
          </li>
          <li class="sub-menu">
            <a href="javascript:;" class="">
              <i class="icon_table"></i>
              <span>Selling Products</span>
              <span class="menu-arrow arrow_carrot-right"></span>
            </a>
            <ul class="sub">
              <li><a class="" href="/all/selling/product">All Selling Product</a></li>
            </ul>
          </li>
          

        </ul>
        <!-- sidebar menu end-->
      </div>
    </aside>
    <!--sidebar end-->

    <!--main content start-->
    <section id="main-content">
      <section class="wrapper">
        <div class="row">
          <div class="col-lg-12">
            <section class="panel">
              <header class="panel-heading">
                Items Table
              </header>
             <c:if test="${!empty itemsList}">
              <table class="table table-striped table-advance table-hover">
                <tbody>
                <tr>
                  <th><i class="icon_id"></i>Id</th>
                  <th><i class="icon_profile"></i>Name</th>
                  <th><i class="icon_cart_alt"></i>Amounts</th>
                  <th><i class="icon_creditcard"></i>Price</th>
                  <th><i class="icon_document_alt"></i>Universal Product Code</th>
                  <th><i class="icon_cogs"></i> Action</th>
                </tr>
                <c:forEach items="${itemsList}" var="item">
                <tr>
                  <td>${item.id}</td>
                  <td>${item.name}</td>
                  <td>${item.amounts}</td>
                  <td>${item.price}</td>
                  <td>${item.universal_product_code}</td>
                  <td>
                    <div class="btn-group">
                      <a class="btn btn-primary" href="/edit/item/${item.id}"><i class="icon_pencil-edit_alt"></i></a>
                      <a class="btn btn-danger" href="/delete/item/${item.id}"><i class="icon_close_alt2"></i></a>
                    </div>
                  </td>
                </tr>
                </c:forEach>
                </tbody>
              </table>
             </c:if>
            </section>
          </div>
        </div>
    </section>
    <!--main content end-->
  </section>
  <!-- container section start -->
  <!-- Javascript files-->
  <jsp:include page="../bootstrapInclude/bootstrap_js.jsp"/>


</body>

</html>
